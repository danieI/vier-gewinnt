import Data.List

-- Typdefinition für Spieler
data Player = Player1 | Player2 deriving (Eq, Show)

-- Typdefinition für Spielfeld
type Board = [[Maybe Player]]

-- Funktion, um ein leeres Spielfeld zu erzeugen
emptyBoard :: Int -> Int -> Board
emptyBoard rows cols = replicate rows (replicate cols Nothing)

-- Funktion, um das Spielfeld auszugeben
printBoard :: Board -> IO ()
printBoard board = putStrLn $ unlines $ map (concatMap printCell) board
  where
    printCell :: Maybe Player -> String
    printCell Nothing = " - "
    printCell (Just Player1) = " X "
    printCell (Just Player2) = " O "

-- Funktion, um einen Spielstein in eine Spalte zu setzen
dropPiece :: Player -> Int -> Board -> Board
dropPiece player col board = transpose $ map dropPiece' $ transpose board
  where
    dropPiece' :: [Maybe Player] -> [Maybe Player]
    dropPiece' column =
      let (top, bottom) = splitAt col column
      in case bottom of
           [] -> top ++ [Just player]
           _:rest -> top ++ (Just player : rest)

-- Funktion, um zu prüfen, ob ein Spieler gewonnen hat
checkWin :: Player -> Board -> Bool
checkWin player board =
  any (checkLine player) $ rows ++ cols ++ diags
  where
    rows = board
    cols = transpose board
    diags = diagonals board
    checkLine :: Player -> [Maybe Player] -> Bool
    checkLine player line =
        any (== 4) $ map length $ filter consecutivePlayer $ group line
        where
            consecutivePlayer :: [Maybe Player] -> Bool
            consecutivePlayer groupedList =
              case groupedList of
                [] -> False
                xs -> head xs == Just player


-- Hilfsfunktion zum Erzeugen der Diagonalen
diagonals :: Board -> [[Maybe Player]]
diagonals board = forwardDiags ++ backwardDiags
  where
    forwardDiags = transpose $ zipWith drop [0 ..] board ++ drop 1 board
    backwardDiags = transpose $ zipWith drop [0 ..] (reverse board) ++ drop 1 (reverse board)

-- Funktion zum Ausführen des Spiels
connectFour :: Int -> Int -> IO ()
connectFour rows cols = playGame Player1 (emptyBoard rows cols)
  where
    playGame :: Player -> Board -> IO ()
    playGame player board = do
      printBoard board
      putStrLn $ "Spieler " ++ show player ++ ", wählen Sie eine Spalte (0-" ++ show (cols - 1) ++ "):"
      col <- getLine
      let colNum = read col :: Int
      if colNum `elem` [0 .. cols - 1] && Nothing `elem` (board !! colNum)
        then do
          let newBoard = dropPiece player colNum board
          if checkWin player newBoard
            then do
              printBoard newBoard
              putStrLn $ "Spieler " ++ show player ++ " hat gewonnen!"
            else if all (\row -> all (/= Nothing) row) newBoard
              then do
                printBoard newBoard
                putStrLn "Unentschieden!"
              else playGame (nextPlayer player) newBoard
        else do
          putStrLn "Ungültiger Zug! Bitte wählen Sie eine gültige Spalte."
          playGame player board

    nextPlayer :: Player -> Player
    nextPlayer Player1 = Player2
    nextPlayer Player2 = Player1

-- Hauptfunktion zum Starten des Spiels
main :: IO ()
main = do
  putStrLn "Willkommen zu 4 Gewinnt!"
  putStrLn "Geben Sie die Anzahl der Zeilen ein:"
  rows <- getLine
  putStrLn "Geben Sie die Anzahl der Spalten ein:"
  cols <- getLine
  connectFour (read rows :: Int) (read cols :: Int)
