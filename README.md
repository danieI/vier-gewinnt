# TODO:
    Ausgabe welcher Speiler dran ist.

    Controllieren ob einer der spieler gewonnen hat.

    Was ist mit unentschieden

    EXTRA:
    Butten um das spiel neu zu starten oder zu beenden.



# Vier Gewinnt - Haskell Projekt ReadMe

## Fensterdefinition

```haskell
window :: Display
window = InWindow "Vier Gewinnt" (1200, 800) (100, 100)
```

- **Display**: Typ für ein grafisches Fenster aus der Gloss-Bibliothek.
- **InWindow**: Ein Konstruktor mit drei Argumenten: Titel, Fenstergröße, Position des Fensters.
- **(1200, 800)**: Größe des Fensters in Pixel.
- **(100, 100)**: Position der linken Ecke des Fensters auf dem Bildschirm.

## Datenmodell

```haskell
data World = World
  { board :: Array (Int, Int) Player
  , currentPlayer :: Player
  , selectedCol :: Int
  , gameWinner :: Maybe Player
  }
```

- **World**: Datentyp, der den Zustand des Spiels beschreibt.
- **board**: Ein 2D-Array, wobei jedes Element ein `Player` ist und mit (Int, Int) die Position des Elements bestimmt wird.
- **currentPlayer**: Spieler, der aktuell am Zug ist.
- **selectedCol**: Speichert die Spalte, in der der Auswahlrahmen vom User steht.
- **gameWinner**: Speichert den eventuellen Gewinner (`Just Player1|Player2`) oder `Nothing`.

## Hauptfunktion

```haskell
main :: IO ()
main = play window background 30 initialWorld draw handleEvent update
```

- **play**: Funktion von der Gloss-Bibliothek, speziell für Spiele gedacht.
- **window**: Erzeugt ein Fenster.
- **background**: Legt die Hintergrundfarbe des Fensters fest.
- **30**: Frames pro Sekunde (fps); hier wird der Spielzustand aktualisiert und neu gezeichnet.
- **initialWorld**: Definiert den Anfangszustand des Spiels `World`.
- **draw**: Generiert eine grafische Darstellung des Spielzustandes in Datentyp `Picture`.
- **handleEvent**: Funktion, die User-Input nimmt und den Spielzustand entsprechend ändert.
- **update**: Funktion, die den Spielzustand nach Zeitintervallen ändert (nicht nötig, da nur User das Spiel beeinflussen).

## Zeichnungsfunktion

### Hauptzeichnung

```haskell
draw :: World -> Picture
draw world = pictures
  [ translate (-300) (-320) $
    pictures ([drawCell x (numRows-1-y) (board world ! (x, y)) for x <- [0..numCols-1], y <- [0..numRows-1]]) ++ [translate (fromIntegral ((selectedCol world * 100 ) + 50)) 300 $ color red $ rectangleWire 100 600])
  , translate (-50) 300 $ color black $ scale 0.2 0.2 $ text statusMessage
  , drawRestartButton
  ]
  where
    statusMessage = case gameWinner world of
      Just Player1 -> "Player Red has won!"
      Just Player2 -> "Player Blue has won!"
      Just Empty -> "Game is a draw!"
      Nothing -> "Player " ++ playerTurn ++ "'s turn"
    playerTurn = case currentPlayer world of
      Player1 -> "Red"
      Player2 -> "Blue"
```

- **pictures**: Funktion, die mehrere `Picture`-Objekte zu einem zusammenfasst.
- **translate**: Ändert die Position von Grafikelementen auf dem Bildschirm.
- **rectangleWire**: Zeichnet ein rechteckiges Drahtmodell zur Visualisierung der Auswahl.

- **drawRestartButton**: Funktion was den Reset-Button zeichnet.

```haskell
pictures (([drawCell x (numRows-1-y) (board world ! (x, y)) | x <- [0..numCols-1], y <- [0..numRows-1]])
```
- Es werden für jeden Stein in abhängigkeit eine Picture erstellt.

```haskell
[translate (fromIntegral ((selectedCol world * 100 ) + 50)) 300 $ color red $ rectangleWire 100 600])
```
- Auswahlrechteck das ich abhängigkeit von selectedCol world auf die richtige Spalte gezeichnet wird.

### Zellenzeichnung

```haskell
drawCell :: Int -> Int -> Player -> Picture
drawCell x y player = translate (fromIntegral x * 100 + 50) (fromIntegral y * 100 + 50) $
                      color (playerColor player) $ circleSolid 40
                      
playerColor :: Player -> Color
playerColor Player1 = light red
playerColor Player2 = light blue
playerColor Empty = black
```

- **fromIntegral**: Konvertiert ganze Zahlen zu einem Gleitkommawert, nötig für Grafikbibliotheken, die Floats oder Doubles erwarten.


## Event Handling

### Haupt-Event-Handling

```haskell
handleEvent :: Event -> World ->

 World
handleEvent (EventKey (MouseButton LeftButton) Down _ (x, y)) world
  | x >= -150 && x <= 50 && y >= 300 && y <= 350 = initialWorld
handleEvent _ world@(World { gameWinner = Just _ }) = world
handleEvent (EventKey (SpecialKey KeyRight) Down _ _) world =
  world { selectedCol = min (numCols - 1) (selectedCol world + 1) }
handleEvent (EventKey (SpecialKey KeyLeft) Down _ _) world =
  world { selectedCol = max 0 (selectedCol world - 1) }
handleEvent (EventKey (SpecialKey KeySpace) Down _ _) world =
  placeStone (selectedCol world) world
handleEvent _ world = world
```

- **EventKey**: Reagiert auf Tastatureingaben und Mausklicks.
- **initialWorld**: Setzt das Spiel zurück zum Anfangszustand, wenn der Reset-Button geklickt wird.
- **placeStone**: Setzt einen Stein in die ausgewählte Spalte, falls möglich.

- **Reset-Button**:
```haskell
handleEvent (EventKey (MouseButton LeftButton) Down _ (x, y)) world
  | x >= -150 && x <= 50 && y >= 300 && y <= 350 = initialWorld
```
Wenn die Maus einen Lnksklick in den x y Berreich drückt wird initalWorld zurück gegeben das das Spiel restarted.

- **Spiel bereits beendet**:
```haskell
handleEvent _ world@(World { gameWinner = Just _ }) = world
```
gameWinner in der World ist nicht mehr Nothing somit hat jemanden gewonnen/unenschieden.

- **Steuerung der ausgewählten Spalte**:
```haskell
handleEvent (EventKey (SpecialKey KeyRight) Down _ _) world =
  world { selectedCol = min (numCols - 1) (selectedCol world + 1) }
```
Nach rechts muss selectedCol von 4 zu 5.  (numCols - 1) = 6 wenn selectedCol = 4 ist (selectedCol world + 1) = 5
Somit wird in selectedCol min zwischen 6 und 5 somit ändert sich die selectedCol von 4 auf 5. 
```haskell
handleEvent (EventKey (SpecialKey KeyLeft) Down _ _) world =
  world { selectedCol = max 0 (selectedCol world - 1) }
```
Nach links muss selectedCol von 5 zu 4.  Die erste Spalte ist 0 wenn selectedCol = 5 ist (selectedCol world - 1) = 4
Somit wird in selectedCol max zwischen 0 und 4 somit ändert sich die selectedCol von 5 auf 4

- **Stein plazieren**:
```haskell
handleEvent (EventKey (SpecialKey KeySpace) Down _ _) world =
  placeStone (selectedCol world) world
```
placestone setzt in der selectedCol einen Stein falls möglich und gibt eine neue World zurück.

## Steinplatzierungsfunktion

### placeStone
```haskell
placeStone :: Int -> World -> World
placeStone col world@(World { gameWinner = Just _ }) = world
placeStone col world =
  case lowestEmptyRow col (board world) of
    Just row ->
      let newBoard = board world // [((col, row), currentPlayer world)]
          hasWon = hasPlayerWon newBoard (currentPlayer world)
          boardFull = isBoardFull newBoard
      in if hasWon
         then world { board = newBoard, gameWinner = Just (currentPlayer world) }
         else if boardFull
              then world { board = newBoard, gameWinner = Just Empty } -- Spiel endet, aber kein Gewinner
              else world { board = newBoard, currentPlayer = if currentPlayer world == Player1 then Player2 else Player1 }
    Nothing -> world
```
```haskell
placeStone col world@(World { gameWinner = Just _ }) = world
```
Wenn es einen gewinner gibt kann auch kein Stein plaziert werden.

```haskell
placeStone col world =
  case lowestEmptyRow col (board world) of  
    Just row ->

    Nothing -> world
```
Wenn man in der Spalte noch einen Stein reinwerfen kann ist es Just Row wenn nicht ist es Nothing und die World wird unverändert zurück gegeben.

```haskell
let newBoard = board world // [((col, row), currentPlayer world)]
    hasWon = hasPlayerWon newBoard (currentPlayer world)
    boardFull = isBoardFull newBoard
```
Es das neue Spielbrett mit den neuen Stein generiert.
und es werden zwei Bool werte erzeugt mit hasWon und boardFull.

```haskell
       in if hasWon
         then world { board = newBoard, gameWinner = Just (currentPlayer world) }
         else if boardFull
              then world { board = newBoard, gameWinner = Just Empty } -- Spiel endet, aber kein Gewinner
              else world { board = newBoard, currentPlayer = if currentPlayer world == Player1
```
- Wenn hasWon = ture dann wir das board zu newboard und gameWinner wird von Nothing zum derzetigen Speiler überschireben.
- Wenn hasWin = false wird erst überprüfut ob baordFull ist.
- Wenn boardFull = true wird board = newBoard und der gameWinnter wird zu Just Empty um somit hat niemand gewonnen.
- Wenn boardFull = false wird Board zu newBoard und der currentPlayer wird getauscht.

### lowestEmptyRow
```haskell
lowestEmptyRow :: Int -> Array (Int, Int) Player -> Maybe Int
lowestEmptyRow col b = listToMaybe [row | row <- [numRows-1,numRows-2..0], b ! (col, row) == Empty]
```
- listToMaybe: Diese Funktion nimmt eine Liste und konvertiert sie in einen Maybe Typ. Wenn die Liste leer ist, gibt sie Nothing zurück, und wenn die Liste mindestens ein Element enthält, gibt sie Just des ersten Elements der Liste zurück.
- b ! (col, row) == Empty:    Hier wird geprüft, ob der Inhalt des Arrays an der Position (col, row) Empty ist. Nur Zeilen, die in der angegebenen Spalte leer sind, werden in die Liste aufgenommen.

### hasPlayerWon
```haskell
hasPlayerWon :: Array (Int, Int) Player -> Player -> Bool
hasPlayerWon board player =
  any id [checkDirection dx dy | dx <- [-1, 0, 1], dy <- [-1, 0, 1], not (dx == 0 && dy == 0)]
  where
    checkDirection dx dy =
      any (checkLine dx dy) [(x, y) | x <- [0..numCols-1], y <- [0..numRows-1]]

    checkLine dx dy (x, y) =
      all inBounds [(x + i * dx, y + i * dy) | i <- [0..3]] &&
      all (\(x', y') -> board ! (x', y') == player) [(x + i * dx, y + i * dy) | i <- [0..3]]

    inBounds (x, y) = x >= 0 && x < numCols && y >= 0 && y < numRows
```

```haskell
any id [checkDirection dx dy | dx <- [-1, 0, 1], dy <- [-1, 0, 1], not (dx == 0 && dy == 0)]
```
any id Ist eine Funktion die Überprüft das mindestens ein Element True ist. 
Bsp:

```haskell
checkingTrue list = any id list        
checkingTrue [False, True, False] --> True
checkingTrue [False, False, False] --> False
```

```haskell
checkDirection dx dy =
  any (checkLine dx dy) [(x, y) | x <- [0..numCols-1], y <- [0..numRows-1]]
```
Die Funktion prüft ob es eine Linie von vier Steinen gibt.

```haskell
checkLine dx dy (x, y) =
  all inBounds [(x + i * dx, y + i * dy) | i <- [0..3]] &&
  all (\(x', y') -> board ! (x', y') == player) [(x + i * dx, y + i * dy) | i <- [0..3]
```
all inBounds: Überprüft, ob alle Zellen in der betrachteten Linie innerhalb der gültigen Grenzen des Spielbretts liegen.


all (\(x', y') -> board ! (x', y') == player): Überprüft, ob alle Zellen in der betrachteten Linie vom angegebenen Spieler besetzt sind.

|   | 0 | 1 | 2 | 3 | 4 | 5 | 6 |
|---|---|---|---|---|---|---|---|
| 0 | . | . | . | . | . | . | . |
| 1 | . | . | . | . | . | . | . |
| 2 | . | . | P2| P1| P1| P1| . |
| 3 | . | . | P1| P1| P1| P2| . |
| 4 | . | . | P2| P2| P1| P1| . |
| 5 | . | . | P1| P1| P2| P2| . |


- Richtungsvektoren: 
Die List Comprehension nimmt alle Kombinationen von dxdx und dydy mit den Werten -1, 0, und 1, außer dx=0dx=0 und dy=0dy=0 (da dies keine Bewegung bedeuten würde). Das gibt uns die Richtungen rechts, links, oben, unten, diagonal nach oben rechts, diagonal nach oben links, diagonal nach unten rechts und diagonal nach unten links.

- CheckDirection: Für jede Zelle auf dem Brett und jede Richtung wird checkLine aufgerufen.
    Zum Beispiel, wenn wir die Position (2, 3) mit Richtung (1, 0) (horizontal nach rechts) prüfen:
    x=2,y=3x=2,y=3, dx=1,dy=0dx=1,dy=0
    Überprüfe die Zellen: (2, 3), (3, 3), (4, 3), (5, 3)
    Diese entsprechen den Positionen von P1 auf unserem Brett, daher würde diese Richtung anzeigen, dass Player1 gewonnen hat.

- CheckLine: Diese Funktion nimmt eine Startposition und eine Richtung und prüft vier aufeinanderfolgende Steine in dieser Richtung.
    Sie prüft erstens, ob alle diese Positionen im Spielbrett gültig sind (inBounds).
    Zweitens prüft sie, ob alle diese Positionen den gleichen Spieler (Player1 oder Player2) enthalten.

- Auswertung und Rückgabe: Wenn any id in der Hauptfunktion mindestens einen wahren Wert findet (was bedeutet, dass es eine Gewinnlinie gibt), gibt die Funktion True zurück; sonst False.



# Daniel Prompt:                            
                            4 gewinnt in Haskell
                            :l main.hs
                            [1 of 2] Compiling Main             ( main.hs, interpreted )
                            main.hs:43:59: error:
                            * Couldn't match type: [Maybe Player]
                            with: Maybe Player
                            Expected: [Maybe Player]
                            Actual: [[Maybe Player]]
                            * In the second argument of `($)', namely `group line'
                            In the second argument of `($)', namely
                            `filter (== Just player) $ group line'
                            In the second argument of `($)', namely
                            `map length $ filter (== Just player) $ group line'
                            |
                            43 |       any (== 4) $ map length $ filter (== Just player) $ group line
                            |                                                           ^^^^^^^^^^
                            Failed, no modules loaded.




# Freddy Prompt:
please create the game four connect with haskell in a terminal version. 2 players should be able to play the game. we have horizontal 7 rows and vertikal 6 rows for the wholes were we can place a stone with color red (player1) or color yellow(player2) (it can be y for yellow and r for red. every player has 1 run then is the next player. do a simple visual view in the terminal version.)   Freddy ist auch am Start.