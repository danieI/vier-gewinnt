import Data.List
import Data.Maybe

type Board = [[Maybe Char]]
type Column = Int
type Row = Int



-- Initialize an empty board
emptyBoard :: Board
emptyBoard = replicate 6 (replicate 7 Nothing)

-- Display the board in the terminal
displayBoard :: Board -> IO ()
displayBoard board = do
  putStrLn " 0 1 2 3 4 5 6"
  mapM_ putStrLn $ reverse $ map (intersperse '|' . map (fromMaybe ' ')) board

-- Check if a column is full
isColumnFull :: Board -> Column -> Bool
isColumnFull board col = isJust (last (board !! col))

-- Place a stone in the specified column
placeStone :: Board -> Column -> Char -> Maybe Board
placeStone board col color
  | col < 0 || col >= length board = Nothing
  | isColumnFull board col = Nothing
  | otherwise = Just $ updateBoard board col color

-- Update the board with the placed stone
updateBoard :: Board -> Column -> Char -> Board
updateBoard board col color = transpose $ map updateRow $ transpose board
  where 
    updateRow row = if length row == col + 1
                    then take col row ++ [Just color] ++ drop (col + 1) row
                    else row


-- Check if there's a win condition
isWin :: Board -> Char -> Bool
isWin board color = any checkDirection directions
  where
    checkDirection (dc, dr) = any (checkLine (0, 0)) [(c, r) | c <- [0..6], r <- [0..5]]
      where
        checkLine (x, y) (cx, cy)
          | x < 0 || x >= 7 || y < 0 || y >= 6 = False
          | fromMaybe ' ' (getCell board x y) == color = checkLine (x + dc, y + dr) (cx, cy)
          | otherwise = cx >= 4
    directions = [(1, 0), (0, 1), (1, 1), (-1, 1)]

-- Get the value of a cell on the board
getCell :: Board -> Column -> Row -> Maybe Char
getCell board col row = (board !! row) !! col

-- Main game loop
main :: IO ()
main = do
  putStrLn "Welcome to Connect Four!"
  playGame emptyBoard 'r'

-- Play the game
playGame :: Board -> Char -> IO ()
playGame board player = do
  displayBoard board
  putStrLn $ "Player " ++ [player] ++ "'s turn. Enter column number (0-6):"
  columnStr <- getLine
  let column = read columnStr :: Int
  case placeStone board column player of
    Nothing -> do
      putStrLn "Invalid move! Please try again."
      playGame board player
    Just newBoard -> do
      if isWin newBoard player
        then do
          displayBoard newBoard
          putStrLn $ "Player " ++ [player] ++ " wins!"
        else do
          let nextPlayer = if player == 'r' then 'y' else 'r'
          playGame newBoard nextPlayer

