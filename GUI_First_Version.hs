module Main where

import Graphics.Gloss
import Graphics.Gloss.Interface.Pure.Game
import Control.Exception (handle)

window :: Display
window = InWindow "Vier Gewinnt!!!" (700, 600) (10, 10)

background :: Color
background = white

data World = World
    { circles :: [[MyCircle]]  -- Changed to use MyCircle
    , currentPlayer :: Player
    }

data Player = Player1 | Player2 deriving (Eq)

initialWorld :: World
initialWorld = World
    { circles = replicate numRows (replicate numCols emptyCircle)
    , currentPlayer = Player1
    }

numRows :: Int
numRows = 6

numCols :: Int
numCols = 7

circleRadius :: Float
circleRadius = 30

emptyCircle :: MyCircle  -- Changed to use MyCircle
emptyCircle = MyCircle white False

data MyCircle = MyCircle  -- Renamed Circle to MyCircle
    { circleColor :: Color
    , filled :: Bool
    }

drawing :: World -> Picture
drawing world = Pictures [headLine, board, circlesPic]
    where
        headLine = Translate (-200) 250 $
            Scale 0.3 0.3 $
            Color red $
            Text $ "Spieler " ++ show (playerToInt (currentPlayer world)) ++ " ist am Zug"
        board = Translate (-10) (-50) $
            Color blue $
            rectangleSolid (fromIntegral (numCols * 2) * circleRadius) (fromIntegral (numRows * 2) * circleRadius)
        circlesPic = Pictures [Translate (fromIntegral (x * 2) * circleRadius - fromIntegral (numCols * round circleRadius)) (fromIntegral (y * 2) * circleRadius - fromIntegral (numRows * round circleRadius)) $
                                    Color (circleColor circle) $
                                    circleSolid circleRadius
                                | (y, row) <- zip [0..] (circles world)
                                , (x, circle) <- zip [0..] row]

playerToInt :: Player -> Int
playerToInt Player1 = 1
playerToInt Player2 = 2

handleInput :: Event -> World -> World
handleInput (EventKey (MouseButton LeftButton) Down _ mousePos) world =
    let col = floor ((fst mousePos + fromIntegral (numCols * round circleRadius)) / (2 * circleRadius))
    in placeCircle (max 0 (min (numCols - 1) col)) world
handleInput _ world = world

placeCircle :: Int -> World -> World
placeCircle col world =
    let (topCircle:rest) = circles world !! col
        newCircles = take col (circles world) ++ [topCircle { circleColor = currentPlayerColor (currentPlayer world), filled = True } : rest] ++ drop (col + 1) (circles world)
        nextPlayer = if currentPlayer world == Player1 then Player2 else Player1
    in world { circles = newCircles, currentPlayer = nextPlayer }

currentPlayerColor :: Player -> Color
currentPlayerColor Player1 = red
currentPlayerColor Player2 = yellow

update :: Float -> World -> World
update _ world = world

main :: IO ()
main = play window background 30 initialWorld drawing handleInput update
