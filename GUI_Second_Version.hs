module Main where

import Graphics.Gloss
import Graphics.Gloss.Interface.Pure.Game
import Data.Array
import Data.List

-- Definition des Fensters
window :: Display
window = InWindow "Vier Gewinnt" (600, 640) (100, 100)

-- Hintergrundfarbe
background :: Color
background = white

-- Anzahl der Reihen und Spalten
numRows, numCols :: Int
numRows = 6
numCols = 7

-- Größe der Spielsteine
cellSize :: Float
cellSize = 80.0

-- Datenstruktur für den Spielzustand
data World = World
  { board :: Array (Int, Int) Player
  , currentPlayer :: Player
  }

data Player = Player1 | Player2 | Empty deriving (Eq)

-- Initialer Zustand des Spielfelds
initialWorld :: World
initialWorld = World
  { board = array indexRange [(index, Empty) | index <- range indexRange]
  , currentPlayer = Player1
  }
  where
    indexRange = ((0, 0), (numCols - 1, numRows - 1))

-- Hauptfunktion
main :: IO ()
main = play window background 30 initialWorld draw handleEvent update

-- Zeichnen des Spiels
draw :: World -> Picture
draw world = translate (fromIntegral (-numCols) * cellSize / 2) (fromIntegral (-numRows) * cellSize / 2) $
             pictures $ [drawCell x y (board world ! (x, y)) | x <- [0..numCols-1], y <- [0..numRows-1]]

drawCell :: Int -> Int -> Player -> Picture
drawCell x y player = translate (fromIntegral x * cellSize) (fromIntegral y * cellSize) $ color (playerColor player) $ circleSolid (cellSize / 2 - 4)

playerColor :: Player -> Color
playerColor Player1 = dark red
playerColor Player2 = dark blue
playerColor Empty = light green

-- Event Handling: Platzieren der Spielsteine
handleEvent :: Event -> World -> World
handleEvent (EventKey (MouseButton LeftButton) Down _ (x, _)) world =
  let col = floor ((x + fromIntegral numCols * cellSize / 2) / cellSize)
  in placeStone col world
handleEvent _ world = world

placeStone :: Int -> World -> World
placeStone col world =
  case lowestEmptyRow col (board world) of
    Just row -> world { board = (board world) // [((col, row), currentPlayer world)]
                      , currentPlayer = if currentPlayer world == Player1 then Player2 else Player1 }
    Nothing -> world

lowestEmptyRow :: Int -> Array (Int, Int) Player -> Maybe Int
lowestEmptyRow col b =
  let column = [b ! (col, row) | row <- [0..numRows-1]]
  in fmap fst . find ((== Empty) . snd) $ zip [0..] column

-- Keine Update-Logik notwendig
update :: Float -> World -> World
update _ = id

-- Spiellogik, Gewinnbedingung etc. können hier hinzugefügt werden

