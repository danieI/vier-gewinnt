-- Running Terminal Version of Four Connected
import Data.List 

-- Typdefinition für Spieler
data Player = Player1 | Player2 deriving (Eq, Show)

-- Typdefinition für Spielfeld
type Board = [[Maybe Player]]

dummyBoard = [[(Just Player2), Nothing, Nothing, Nothing, Nothing, Nothing, Nothing], [(Just Player1), Nothing, Nothing, Nothing, Nothing, Nothing, Nothing], [(Just Player2), Nothing, Nothing, Nothing, Nothing, Nothing, Nothing], [(Just Player1), Nothing, Nothing, Nothing, Nothing, Nothing, Nothing], [(Just Player2), Nothing, Nothing, Nothing, Nothing, Nothing, Nothing], [(Just Player1) ,(Just Player2), Nothing, Nothing, Nothing, Nothing, Nothing]]
dummyEmptyBoard = emptyBoard 6 7

-- Funktion, um ein leeres Spielfeld zu erzeugen
emptyBoard :: Int -> Int -> Board
emptyBoard rows cols = replicate rows (replicate cols Nothing)

-- Funktion, um das Spielfeld auszugeben
printBoard :: Board -> IO ()
printBoard board = putStrLn $ unlines $ map (concatMap printCell) board
  where
    printCell :: Maybe Player -> String
    printCell Nothing = " - "
    printCell (Just Player1) = " X "
    printCell (Just Player2) = " O "

-- Funktion, um einen Spielstein in eine Spalte zu setzen
dropPiece :: Player -> Int -> Board -> Board
dropPiece player col board = transpose $ map dropPiece' $ transpose board
  where
    dropPiece' :: [Maybe Player] -> [Maybe Player]
    dropPiece' column =
      let (top, bottom) = splitAt col column
      in case bottom of
           [] -> top ++ [Just player]
           _:rest -> top ++ (Just player : rest)

dropPieceAndi :: Player -> Int -> Board -> Board
dropPieceAndi player col board
  | (head board !! col) /= Nothing = board
  | otherwise = transpose $ dropPieceAndi' player col 0 (transpose board)

dropPieceAndi' :: Player -> Int -> Int -> Board -> Board
dropPieceAndi' player col index board
  | length board == 0 = []
  | (col == 0 ) = dropPieceAndiOne player (head board) : tail board
  | otherwise = head board : dropPieceAndi' player (col - 1) (index) (tail board)  

dropPieceAndiOne :: Player -> [Maybe Player] -> [Maybe Player]
dropPieceAndiOne player row
  | last row == Nothing = init row ++ [Just player]  
  | otherwise = dropPieceAndiOne player (init row) ++ [last row]  

-- Funktion, um zu prüfen, ob ein Spieler gewonnen hat
checkWin :: Player -> Board -> Bool
checkWin player board =
  any (checkLine player) $ rows ++ cols ++ diags
  where
    rows = board
    cols = transpose board
    diags = diagonals board
    checkLine :: Player -> [Maybe Player] -> Bool
    checkLine player line =
        any (== 4) $ map length $ filter consecutivePlayer $ group line
        where
            consecutivePlayer :: [Maybe Player] -> Bool
            consecutivePlayer groupedList =
                case groupedList of
                    [] -> False
                    xs -> head xs == Just player


-- Hilfsfunktion zum Erzeugen der Diagonalen
diagonals :: Board -> [[Maybe Player]]
diagonals board = forwardDiags ++ backwardDiags
  where
    forwardDiags = transpose $ zipWith drop [0 ..] board ++ drop 1 board
    backwardDiags = transpose $ zipWith drop [0 ..] (reverse board) ++ drop 1 (reverse board)

-- Funktion zum Ausführen des Spiels
connectFour :: Int -> Int -> IO ()
connectFour rows cols = playGame Player1 (emptyBoard rows cols)
  where
    playGame :: Player -> Board -> IO ()
    playGame player board = do
      printBoard board
      putStrLn $ "Spieler " ++ show player ++ playerSymbol player ++ ", wählen Sie eine Spalte (0-" ++ show (cols - 1) ++ "):"
      col <- getLine
      let colNum = read col :: Int
      if colNum `elem` [0 .. cols - 1]
        then do
          let newBoard = dropPieceAndi player colNum board
          if newBoard /= board
            then do
            if checkWin player newBoard
              then do
                printBoard newBoard
                putStrLn $ "Spieler " ++ show player ++ " hat gewonnen!"
              else if all (\row -> all (/= Nothing) row) newBoard
                then do
                  printBoard newBoard
                  putStrLn "Unentschieden!"
                else playGame (nextPlayer player) newBoard
            else do
              putStrLn "Ungültiger Zug! Bitte wählen Sie eine gültige Spalte."
              playGame player board 
        else do
          putStrLn "Ungültiger Zug! Bitte wählen Sie eine gültige Spalte."
          playGame player board

    nextPlayer :: Player -> Player
    nextPlayer Player1 = Player2
    nextPlayer Player2 = Player1

    playerSymbol :: Player -> String
    playerSymbol Player1 = " (X) "
    playerSymbol Player2 = " (O) "


--Checking if not any other Funktion is funking the droping up!!
connectFour' :: Int -> Int -> IO ()
connectFour' rows cols = playGame Player1 (emptyBoard rows cols)
  where
    playGame :: Player -> Board -> IO ()
    playGame player board = do
      printBoard board
      putStrLn $ "Spieler " ++ show player ++ playerSymbol player ++ ", wählen Sie eine Spalte (0-" ++ show (cols - 1) ++ "):"
      col <- getLine
      let colNum = read col :: Int
      if colNum `elem` [0 .. cols - 1] && Nothing `elem` (board !! colNum)
        then do
          let newBoard = dropPiece player colNum board
          printBoard newBoard
      else do
          putStrLn "Ungültiger Zug! Bitte wählen Sie eine gültige Spalte."
          playGame player board
    
    playerSymbol :: Player -> String
    playerSymbol Player1 = " (X) "
    playerSymbol Player2 = " (O) "
        


-- Hauptfunktion zum Starten des Spiels
main :: IO ()
main = do
  putStrLn "Willkommen zu 4 Gewinnt!"
  connectFour (6 :: Int) (7 :: Int)
