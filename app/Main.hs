module Main where

import Graphics.Gloss
import Graphics.Gloss.Interface.Pure.Game
import Data.Array
import Data.Maybe (listToMaybe)

-- Definition des Fensters
window :: Display
window = InWindow "Vier Gewinnt" (1200, 800) (100, 100)

-- Hintergrundfarbe
background :: Color
background = white

-- Anzahl der Reihen und Spalten
numRows, numCols :: Int
numRows = 6
numCols = 7

-- Größe der Spielsteine
cellSize :: Float
cellSize = 80.0

-- Datenstruktur für den Spielzustand
data World = World
  { board :: Array (Int, Int) Player
  , currentPlayer :: Player
  , selectedCol :: Int
  , gameWinner :: Maybe Player
  }

data Player = Player1 | Player2 | Empty deriving (Eq, Show)

-- Initialer Zustand des Spielfelds
initialWorld :: World
initialWorld = World
  { board = array indexRange [(index, Empty) | index <- range indexRange]
  , currentPlayer = Player1
  , selectedCol = 0
  , gameWinner = Nothing
  }
  where
    indexRange = ((0, 0), (numCols - 1, numRows - 1))

-- Hauptfunktion
main :: IO ()
main = play window background 30 initialWorld draw handleEvent update

-- Zeichnen des Spiels
draw :: World -> Picture
draw world = pictures
  [ translate (-300) (-320) $
    pictures (([drawCell x (numRows-1-y) (board world ! (x, y)) | x <- [0..numCols-1], y <- [0..numRows-1]]) ++ [translate (fromIntegral ((selectedCol world * 100 ) + 50)) 300 $ color red $ rectangleWire 100 600])
  , translate (-50) 300 $ color (playerColor (currentPlayer world)) $ scale 0.2 0.2 $ text statusMessage
  , drawRestartButton
  ]
  where
    statusMessage = case gameWinner world of
      Just Player1 -> "Player Red has won!"
      Just Player2 -> "Player Blue has won!"
      Just Empty -> "Game is a draw!"
      Nothing -> "Player " ++ playerTurn ++ "'s turn"
    playerTurn = case currentPlayer world of
      Player1 -> "Red"
      Player2 -> "Blue"

drawRestartButton :: Picture
drawRestartButton = translate (-200) 350 $ color (makeColor 0 0.5 0 1) $ pictures [
    rectangleSolid 200 50,
    translate (-80) (-15) $ color white $ scale 0.2 0.2 $ text "Restart"
  ]


drawCell :: Int -> Int -> Player -> Picture
drawCell x y player = translate (fromIntegral x * 100 + 50) (fromIntegral y * 100 + 50) $
                      color (playerColor player) $ circleSolid 40


playerColor :: Player -> Color
playerColor Player1 = light red
playerColor Player2 = light blue
playerColor Empty = black

-- Event Handling
handleEvent :: Event -> World -> World
handleEvent (EventKey (MouseButton LeftButton) Down _ (x, y)) world
  | x >= -300 && x <= -100 && y >= 325 && y <= 400 = initialWorld  -- Anpassen der Koordinaten des Restart-Buttons
handleEvent _ world@(World { gameWinner = Just _ }) = world
handleEvent (EventKey (SpecialKey KeyRight) Down _ _) world =
  world { selectedCol = min (numCols - 1) (selectedCol world + 1) }
handleEvent (EventKey (SpecialKey KeyLeft) Down _ _) world =
  world { selectedCol = max 0 (selectedCol world - 1) }
handleEvent (EventKey (SpecialKey KeySpace) Down _ _) world =
  placeStone (selectedCol world) world
handleEvent _ world = world

placeStone :: Int -> World -> World
placeStone col world@(World { gameWinner = Just _ }) = world
placeStone col world =
  case lowestEmptyRow col (board world) of
    Just row ->
      let newBoard = board world // [((col, row), currentPlayer world)]
          hasWon = hasPlayerWon newBoard (currentPlayer world)
          boardFull = isBoardFull newBoard
      in if hasWon
         then world { board = newBoard, gameWinner = Just (currentPlayer world) }
         else if boardFull
              then world { board = newBoard, gameWinner = Just Empty } -- Spiel endet, aber kein Gewinner
              else world { board = newBoard, currentPlayer = if currentPlayer world == Player1 then Player2 else Player1 }
    Nothing -> world


lowestEmptyRow :: Int -> Array (Int, Int) Player -> Maybe Int
lowestEmptyRow col b = listToMaybe [row | row <- [numRows-1,numRows-2..0], b ! (col, row) == Empty]

isBoardFull :: Array (Int, Int) Player -> Bool
isBoardFull board = all (/= Empty) (elems board)

hasPlayerWon :: Array (Int, Int) Player -> Player -> Bool
hasPlayerWon board player =
  any id [checkDirection dx dy | dx <- [-1, 0, 1], dy <- [-1, 0, 1], not (dx == 0 && dy == 0)]
  where
    checkDirection dx dy =
      any (checkLine dx dy) [(x, y) | x <- [0..numCols-1], y <- [0..numRows-1]]

    checkLine dx dy (x, y) =
      all inBounds [(x + i * dx, y + i * dy) | i <- [0..3]] &&
      all (\(x', y') -> board ! (x', y') == player) [(x + i * dx, y + i * dy) | i <- [0..3]]

    inBounds (x, y) = x >= 0 && x < numCols && y >= 0 && y < numRows


-- Keine Update-Logik notwendig
update :: Float -> World -> World
update _ = id

